//modulos internos
const express = require("express");
const router = express.Router();

//modulos propios
//definimos la clase usuario
const { Usuario } = require("../model/usuario");
//definimos la clase mascota
const { Mascota } = require("../model/mascota");

const auth = require("../middleware/auth");


//Rutas de mascotas aqui  hay un  cambio
//post por que vamos a enviar 
router.post ("/",auth,async(req,res) =>{
   //tomar  el usuario si  existe
   const  usuario  =  await  Usuario.findById(req.usuario._id);
   //si  el  usuario no existe
   if (!usuario) return res.status(400).send("El usuario no existe");
   //consultar si el tipo existe
   let  mascotaTipo  =  await  Mascota.findOne({ tipo: req.body.tipo });
  // si la mascota existe
   if (mascotaTipo) return res.status(400).send("El tipo de mascota ya existe inserta otro");
   const mascota = new Mascota({
       idUsuario: usuario._id,
       nombre: req.body.nombre,
       tipo: req.body.tipo,
       descripcion: req.body.descripcion,
   });
   
   const result = await mascota.save();
   res.status(200).send(result);
});

module.exports = router;