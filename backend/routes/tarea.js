//modulos internos
const express = require("express");
const router = express.Router();

//modulos propios
//definimos la clase usuario
const { Usuario } = require("../model/usuario");
//definimos la clase tarea
const { Tarea } = require("../model/tarea");

const auth = require("../middleware/auth");


//Rutas de tareas aqui  hay un  cambio
//post por que vamos a enviar 
router.post ("/",auth,async(req,res) =>{
   //tomar  el usuario si  existe
   const  usuario  =  await  Usuario.findById(req.usuario._id);
   //si  el  usuario no existe
   if (!usuario) return res.status(400).send("El usuario no existe");
   const tarea = new Tarea({
       idUsuario: usuario._id,
       nombre: req.body.nombre,
       descripcion: req.body.descripcion,
       estado: req.body.estado,
   });
   
   const result = await tarea.save();
   res.status(200).send(result);
});
   
//listar tareas  --------------------------------
router.get("/lista", auth, async (req,res) =>{
 // tomar nuestro usuario logeado
 //traer el id del usuario
 //findById encontrar por el  di
 const usuario = await Usuario.findById(req.usuario._id);
 //si el usuario no existe
 if (!usuario) return res.status(400).send("El usuario no existe!!");
 //si el usuario existe en la bd tomamos sus tareas
    const tarea = await Tarea.find({ idUsuario: req.usuario._id});
    res.send(tarea);    
});

//Editar tarea ------------------------------------------
router.put("/", auth, async(req,res)=>{
    //traer el id del usuario
    const usuario = await Usuario.findById(req.usuario._id);
    //si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe!!");    
    // si el  usuario existe se realiza el update
    const tarea =  await Tarea.findByIdAndUpdate(
     req.body._id,
     {
      idUsuario: usuario._id,
      nombre: req.body.nombre,
      descripcion: req.body.descripcion,
      estado: req.body.estado,   
     },
     {
         //para refrescar el objeto actualizado para que muestre en el momneto
         new:true,
     }
    );
    //si la tarea no es encontrada
    if(!tarea) return  res.status(400).send("No hay tareas");
    //si existen  tareas enviamos respuesta
    res.status(200).send(tarea);
});

//Eliminar una tarea del usuario registrado se manda el id por la url  para eliminar
router.delete("/:_id", auth, async(req,res)=>{
    //traer el id del usuario
    const usuario = await Usuario.findById(req.usuario._id);
    //si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe!!"); 

    const tarea = await Tarea.findOneAndDelete(req.params._id);
    //si no encuentra la tarea
    if(!tarea) return res.status(400).send("No hay tareas asignadas");
    //se muestra el mensaje
    res.status(200).send({message:"Tarea Eliminada"});
});


//exports
module.exports = router;