//modulos internos

const mongoose =  require("mongoose");

//esquema tarea

const esquemaTarea = new mongoose.Schema({
    //creamos el json del esquema tarea
    idUsuario:String,
    nombre:String,
    descripcion:String,
    estado:String,
    fecha:{
        type:Date,
        default: Date.now,
    },
});

//exports
const Tarea = mongoose.model("tarea",esquemaTarea);
module.exports.Tarea = Tarea;